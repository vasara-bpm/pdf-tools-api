# camunda-external-task-client

Opinionated Camunda external task client template

Environment variables, general parameters:
| Option | Description | Type | Default value |
| ------ | ------ | ------ | ------ |
| LOGGING_LEVEL | Level to use logging, as defined by 'winston'. Passed to Camunda client logger. NOTE! if level 'http' is used, it's passed to camunda as 'info' | text | 'info' |
| MONITORPORT | The port that the heatlcheck http server listens to | number | 3000 |
| HBALERTLIMIT | Alert limit in seconds for Heartbeat monitoring. If no Heartbeat message is processed within kimit, Healtcheck service status changes to error  | number | 3600 |

Environment variables, connection to camunda:
| Option | Description | Type | Default value |
| ------ | ------ | ------ | ------ |
| BPMENGINEURL | Path to the camunda rest engine api | text | 'http://localhost:8080/engine-rest' |
| TASKCLIENTTIMEOUT | Camunda client Long Polling timeout in milliseconds. | number | 10000 |
| AUTHORIZATION_TOKEN | Bearer token to be attached to Camunda connection requests | text | '1234' |
| QUERYREQUESTSTATUSTOPIC | name of the topic, that the client polling request status from SN listens to | text | 'sendServiceRequest' |
| QUERYREQUESTSTATUSWORKERID | Worker id for the client the client polling request status from ServiceNow | text | 'some-random-id' |
| RETRYDELAY | Delay in m illiseconds that iw waited before retry of a failed task | number | 1000 |

# Docker

Build Docker image

```bash
$ docker build . -t snow-external-task-client:latest

```

Try out the above image with host networking

```
$ docker run --rm -ti --network host snow-external-task-client:latest
```
