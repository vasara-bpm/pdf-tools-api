import fs from 'fs';
import path from 'path';

import * as camunda from 'camunda-external-task-client-js';
import * as dotenv from 'dotenv';
import * as rest from 'typed-rest-client';
import * as httpc from 'typed-rest-client/HttpClient';
import BearerTokenAuthInterceptor from 'vasara-bearer-token-interceptor';
import * as winston from 'winston';

dotenv.config();
export const LOG_LEVEL = process.env.LOGGING_LEVEL || 'debug';
export const TEMPLATE_DIR = process.env.TEMPLATE_DIR || path.join(__dirname, 'workers', 'templates');
export const ENGINE_URL = process.env.BPM_ENGINE_URL || 'http://localhost:8080/engine-rest';
export const PDF_TOOLS_API_URL = process.env.PDF_TOOLS_API_URL || 'http://localhost:8082/api/v1';
export const HB_MONITOR_PORT = process.env.MONITOR_PORT || 3000;
export const HB_ALERT_LIMIT = parseInt(process.env.HB_ALERT_LIMIT || '3600', 10);
export const TASK_CLIENT_TIMEOUT = parseInt(process.env.TASK_CLIENT_TIMEOUT || '10000', 10) || 10000;
export const TASK_CLIENT_MAX_TASKS = parseInt(process.env.TASK_CLIENT_MAX_TASKS || '10', 10) || 10;
export const TOPIC_PDF_FORM_FILL = process.env.PDF_FORM_FILL || 'pdf.form.fill';
export const TOPIC_PDF_FORM_FILL_COPIES = process.env.PDF_FORM_FILL_COPIES || 'pdf.form.fill.copies';
export const TOPIC_PDF_PDF_A_CONVERT = process.env.PDF_PDF_A_CONVERT || 'pdf.pdfa.convert';
export const WORKER_ID = process.env.WORKER_ID || 'pdf-tools-client';
export const AUTH_TOKEN = process.env.AUTH_TOKEN || 'vasara-rest-secret';

export const loggerFormat = winston.format.printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});
export const loggerWithLabel = (label: string) =>
  winston.createLogger({
    level: LOG_LEVEL,
    format: winston.format.combine(winston.format.label({ label }), winston.format.timestamp(), loggerFormat),
    transports: [new winston.transports.Console()],
  });

export const camundaClient = new camunda.Client({
  baseUrl: ENGINE_URL,
  workerId: WORKER_ID,
  maxTasks: TASK_CLIENT_MAX_TASKS,
  maxParallelExecutions: TASK_CLIENT_MAX_TASKS,
  asyncResponseTimeout: TASK_CLIENT_TIMEOUT,
  interceptors: [new BearerTokenAuthInterceptor(AUTH_TOKEN)],
  // logger.level is required to construct a logger at a non-default level
  // but isn't in the types module
  // @ts-ignore
  use: camunda.logger.level(LOG_LEVEL === 'http' ? 'info' : LOG_LEVEL),
});

export const pdfToolsRestClient = new rest.RestClient('camunda', PDF_TOOLS_API_URL, []);
// REST client only handles json content,
// so we need HTTP client for handling files
export const httpClient = new httpc.HttpClient('camunda');
