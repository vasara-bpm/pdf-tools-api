// Main module to start all Integration components in Vasara BPM - Sarake Sign integration

import { IncomingMessage, ServerResponse, createServer } from 'http';

import * as cfg from './config';
import { HeartbeatState } from './types/client';
import ConvertToPdfA from './workers/ConvertToPdfA';
import FillPdfForm from './workers/FillPdfForm';
import FillPdfFormCopies from './workers/FillPdfFormCopies';

const heartbeat: HeartbeatState = {
  requestHeartbeatTimestamp: new Date(),
  queryHeartbeatTimestamp: new Date(),
};

const logger = cfg.loggerWithLabel('PdfToolsApi');

const heartbeatMonitor = new Promise((resolve, reject) => {
  const hbRequestListener = (req: IncomingMessage, res: ServerResponse) => {
    if (cfg.HB_ALERT_LIMIT == 0) {
      res.writeHead(200);
      res.end('{"status": "ok"}');
      return;
    }
    const sendHbDelta =
      heartbeat.requestHeartbeatTimestamp.getTime() + cfg.HB_ALERT_LIMIT * 1000 - new Date().getTime();
    const queryHbDelta = heartbeat.queryHeartbeatTimestamp.getTime() + cfg.HB_ALERT_LIMIT * 1000 - new Date().getTime();
    if (sendHbDelta > 0 && queryHbDelta > 0) {
      res.writeHead(200);
      res.end('{"status": "ok"}');
    } else {
      res.writeHead(500);
      res.end('{"status": "error"}');
    }
  };

  const server = createServer(hbRequestListener);
  server.listen(cfg.HB_MONITOR_PORT);
});

Promise.all([FillPdfForm(), FillPdfFormCopies(), ConvertToPdfA(), heartbeatMonitor])
  .then(values => {
    for (const value of values) {
      if (value) {
        logger.info(value);
      }
    }
  })
  .catch(error => {
    process.exit(1); // fail fast
  });
