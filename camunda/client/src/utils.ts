// Async iterator sugar for subscription
import { Client, HandlerArgs, Task, TaskService, TypedValue } from 'camunda-external-task-client-js';
import { Logger } from 'winston';

export async function* subscribe(client: Client, topic: string, options?: any): AsyncIterable<HandlerArgs> {
  // Define asynchronous lock
  const locker: (() => void)[] = [];
  const Lock = () =>
    new Promise<void>(resolve => {
      locker.push(resolve);
    });

  // Subscribe topic, push to queue, release lock
  const queue: HandlerArgs[] = [];
  const subscription = client.subscribe(topic, options || {}, async function (payload) {
    queue.push(payload);
    let release;
    while ((release = locker.shift())) release();
  });

  // Consume queue, wait for lock
  let payload;
  try {
    while (true) {
      while ((payload = queue.shift())) {
        yield payload;
      }
      await Lock();
    }
  } finally {
    process.exit(1); // fail fast
  }
}

/** Get a variable from a task, and send an error message to Camunda and log
 * it if it doesn't exist or doesn't have the right type. */
export const requireVar = async (
  task: Task,
  taskService: TaskService,
  logger: Logger,
  key: string,
  ty: string
): Promise<TypedValue | undefined> => {
  const v = task.variables.getTyped(key);
  const errorMessage = (() => {
    if (!v || v.value === null) {
      return `Missing required variable ${key}`;
    }
    if (v.type !== ty) {
      return `Variable ${key} was mistyped (wanted ${ty}, had ${v.type})`;
    }
    return undefined;
  })();

  if (errorMessage) {
    logger.error(errorMessage);
    await taskService.handleFailure(task, {
      errorMessage,
      retries: 0,
      retryTimeout: 60000,
    });
    return undefined;
  }
  return v;
};
