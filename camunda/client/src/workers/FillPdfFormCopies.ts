import fs from 'fs';
import path from 'path';
import { Readable } from 'stream';

import { Variables } from 'camunda-external-task-client-js';
import concatStream from 'concat-stream';
import FormData from 'form-data';

import * as cfg from '../config';
import { PDF_TOOLS_API_URL, TEMPLATE_DIR } from '../config';
import * as api from '../types/pdf';
import { subscribe } from '../utils';

type FormFieldPart = api.components['schemas']['FormFieldPart'];

const logger = cfg.loggerWithLabel('FillPdfForm');

const FillPdfForm = async () => {
  let availableTemplates = fs.readdirSync(TEMPLATE_DIR).filter(filename => filename.endsWith('.pdf'));
  logger.info(`Available templates: ${availableTemplates}`);

  for await (const { task, taskService } of subscribe(cfg.camundaClient, cfg.TOPIC_PDF_FORM_FILL_COPIES)) {
    // Read all variables to use them as variables to fill
    const fields: FormFieldPart = task.variables.getAll();
    // Read template
    const templateName = fields?.['template'];
    availableTemplates = fs.readdirSync(TEMPLATE_DIR).filter(filename => filename.endsWith('.pdf'));
    if (!availableTemplates.includes(templateName)) {
      logger.info(`Available templates: ${availableTemplates}`);
      await taskService.handleFailure(task, {
        errorMessage: `Template not found: ${templateName}`,
        errorDetails: `Template ${templateName} not any of ${availableTemplates}`,
        retries: 0,
      });
      continue;
    }
    const pdf = fs.readFileSync(path.join(TEMPLATE_DIR, templateName));
    // Ensure date field array
    const arrayOfFieldMaps: Record<string, any>[] = fields?.['data'] as any;
    if (!Array.isArray(fields) || fields.length < 1) {
      await taskService.handleFailure(task, {
        errorMessage: `Variable 'data' not found or not valid`,
        errorDetails: `Filling many copies requires variable 'data' with array of field mappings. Got '${
          typeof fields !== 'undefined' ? JSON.stringify(fields) : 'undefined'
        }' instead`,
        retries: 0,
      });
    }
    // Format date variables
    for (const fieldMap of arrayOfFieldMaps) {
      for (const name of Object.keys(fields)) {
        if (fieldMap[name] instanceof Date) {
          fieldMap[name] = (fieldMap[name] as Date).toISOString().split('T')[0];
        }
      }
    }

    // Build request
    const form = new FormData();
    form.append('pdf', pdf, { filename: templateName, contentType: 'application/pdf', knownLength: pdf.length });
    form.append('fields', JSON.stringify(arrayOfFieldMaps));
    const stream = new Readable({
      read() {
        this.push(form.getBuffer());
        this.push(null);
      },
    });
    // Call API
    const fileResp = await cfg.httpClient.request('POST', PDF_TOOLS_API_URL + '/fill', stream, form.getHeaders());
    // Save response
    try {
      if (fileResp.message.statusCode === 200) {
        const fileBuf: Buffer = await new Promise((resolve, reject) => {
          const stream = concatStream(resolve);
          fileResp.message.on('error', reject);
          fileResp.message.pipe(stream);
        });
        const localVariables = new Variables();
        localVariables.setTyped('output', {
          type: 'file',
          value: fileBuf.toString('base64'),
          valueInfo: {
            filename: templateName,
            mimetype: 'application/pdf',
            mimeType: 'application/pdf',
          },
        });
        await taskService.complete(task, new Variables(), localVariables);
      } else {
        logger.error(`Service responded error: ${fileResp.message.statusCode}`);
        await taskService.handleFailure(task, {
          errorMessage: `Service responded error: ${fileResp.message.statusCode}`,
          errorDetails: `${fileResp.message}`,
          retries: 0,
          retryTimeout: 60000,
        });
      }
    } catch (e: any) {
      logger.error(`Error connecting: ${e.message}`);
      await taskService.handleFailure(task, {
        errorMessage: 'Error connecting to service',
        errorDetails: e.message,
        retries: 2,
        retryTimeout: 60000,
      });
    }
  }
};

export default FillPdfForm;
