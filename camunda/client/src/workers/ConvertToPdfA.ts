import { Readable } from 'stream';

import { Variables } from 'camunda-external-task-client-js';
import concatStream from 'concat-stream';
import FormData from 'form-data';

import * as cfg from '../config';
import { PDF_TOOLS_API_URL } from '../config';
import { requireVar, subscribe } from '../utils';

const logger = cfg.loggerWithLabel('ConvertToPdfA');

const ConvertToPdfA = async () => {
  for await (const { task, taskService } of subscribe(cfg.camundaClient, cfg.TOPIC_PDF_PDF_A_CONVERT)) {
    // Read variables
    const inputVar = await requireVar(task, taskService, logger, 'input', 'file');
    if (!inputVar) {
      // requireVar already fails the task
      continue;
    }

    // TODO: camunda-external-task-client-nodejs expects files to be process global
    if (inputVar?.value) {
      inputVar.value.remotePath = `/execution/${task.executionId}/localVariables/input/data`;
    }
    const pdf = await inputVar?.value.load();
    // Build request
    const form = new FormData();
    form.append('pdf', pdf.content, { filename: 'input.pdf', contentType: 'application/pdf', knownLength: pdf.length });
    const stream = new Readable({
      read() {
        this.push(form.getBuffer());
        this.push(null);
      },
    });
    const fileResp = await cfg.httpClient.request('POST', PDF_TOOLS_API_URL + '/convert', stream, form.getHeaders());
    try {
      if (fileResp.message.statusCode === 200) {
        const fileBuf: Buffer = await new Promise((resolve, reject) => {
          const stream = concatStream(resolve);
          fileResp.message.on('error', reject);
          fileResp.message.pipe(stream);
        });
        const localVariables = new Variables();
        localVariables.setTyped('output', {
          type: 'file',
          value: fileBuf.toString('base64'),
          valueInfo: {
            filename: pdf.filename,
            mimetype: 'application/pdf',
            mimeType: 'application/pdf',
          },
        });
        await taskService.complete(task, new Variables(), localVariables);
      } else {
        logger.error(`Service responded error: ${fileResp.message.statusCode}`);
        let errorDetails = await fileResp.readBody();
        try {
          errorDetails = JSON.parse(errorDetails).message;
        } catch (e) {
          // return the whole body
        }
        await taskService.handleFailure(task, {
          errorMessage: `Service responded error: ${fileResp.message.statusCode}`,
          errorDetails: errorDetails,
          retries: 0,
          retryTimeout: 60000,
        });
      }
    } catch (e: any) {
      logger.error(`Error connecting: ${e.message}`);
      await taskService.handleFailure(task, {
        errorMessage: 'Error connecting to service',
        errorDetails: e.message,
        retries: 2,
        retryTimeout: 60000,
      });
    }
  }
};

export default ConvertToPdfA;
