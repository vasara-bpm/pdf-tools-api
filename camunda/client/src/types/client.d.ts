export interface HeartbeatState {
  requestHeartbeatTimestamp: Date;
  queryHeartbeatTimestamp: Date;
}

export interface Key {
  keyName: string;
  keyOperator: '=';
  keyValue: string;
}

export interface Error {
  errorMessage: string;
  errorDetails: string;
  retries: number;
}
