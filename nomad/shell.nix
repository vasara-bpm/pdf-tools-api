{ pkgs ? import ../nix {}
, unstable ? import ../nix { nixpkgs = sources."nixpkgs-unstable"; }
, sources ? import ../nix/sources.nix {}
, api_jar ? import ./.. {}
, client_build ? import ./../camunda/client {}
}:

let

  icc_profile = ../api/src/main/resources/AdobeRGB1998.icc;
  converter = ../api/src/main/resources/converter;

in

pkgs.mkShell {
  buildInputs = with pkgs; [
    unstable.nomad_1_0
    client_build
    adoptopenjdk-jre-hotspot-bin-13
    gawk
    ghostscript
    glibc.bin
    gnugrep
    gnused
    netcat
    jq
  ];
  shellHook = ''
    export JVM_OPTS="-Dfile.encoding=UTF-8 -Duser.language=fi -Duser.country=FI -Xmx768m -XX:MaxMetaspaceSize=256m -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5006"
    export JAR=${api_jar}
    export SHELL=${pkgs.bashInteractive}/bin/bash
    export CONVERTER=${converter}
    export ICC_PROFILE=${icc_profile}
    export GS_FONTPATH=${pkgs.google-fonts}/share/fonts/truetype
    export NOMAD_PORT_camunda_http=$(nomad alloc status -json $(nomad status vasara|grep Allocations -A2|tail -n1|awk '{ print $1 }')|jq ".AllocatedResources.Shared.Networks[].DynamicPorts[]|select(.Label==\"camunda_http\")|.Value")
  '';
}
