job "pdf-tools" {
  datacenters = ["dc1"]
  type = "batch"

  reschedule {
    attempts = 0
    interval = "1m"
    delay    = "15s"
    unlimited = true
  }

  meta {
    shell_nix = abspath("./shell.nix")
    vasara_rest_secret = "vasara-rest-secret"
  }

  group "development" {

    network {
      port "api_http" {}
      port "client_http" {}
    }

    task "api" {
      driver = "raw_exec"
      env {
        HTTP_PORT = "${NOMAD_PORT_api_http}"
        SHELL_NIX = "${NOMAD_META_shell_nix}"
      }
      config {
        command = "/bin/sh"
        args = [
          "-c",
          <<EOH
nix-shell $SHELL_NIX --run "
java \$JVM_OPTS -jar \$JAR
"
EOH
        ]
      }
    }

    task "client" {
      driver = "raw_exec"
      env {
        SHELL_NIX = "${NOMAD_META_shell_nix}"
        MONITOR_PORT= "${NOMAD_PORT_client_http}"
        PDF_TOOLS_API_URL = "http://localhost:${NOMAD_PORT_api_http}/api/v1"
        AUTH_TOKEN = "vasara-rest-secret"
      }
      config {
        command = "/bin/sh"
        args = [
          "-c",
          <<EOH
nix-shell $SHELL_NIX --run "
# NOMAD_PORT_camunda_http is resolved in shell.nix
export BPM_ENGINE_URL=http://localhost:\$NOMAD_PORT_camunda_http/engine-rest
app
"
EOH
        ]
      }
    }
  }
}
