{
  description = "Vasara PDF Tools";

  nixConfig = {
    extra-trusted-public-keys = "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4=";
    extra-substituters = "https://vasara-bpm.cachix.org";
  };

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/release-24.05";
    nixpkgs-maven.url = "github:NixOS/nixpkgs/release-22.11";
    # https://bugs.ghostscript.com/show_bug.cgi?id=707450
    nixpkgs-ghostscript.url = "github:NixOS/nixpkgs/release-23.11";
    # ^ 10.02.x is good, 10.03.x is bad for PDF/A-2b
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    npmlock2nix = {
      url = "github:nix-community/npmlock2nix";
      flake = false;
    };
    vasara-bpm = {
      url = "gitlab:vasara-bpm/pkgs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    mvn2nix = {
      url = "github:fzakaria/mvn2nix";
      inputs.nixpkgs.follows = "nixpkgs-maven";
      inputs.utils.follows = "flake-utils";
    };
  };

  outputs =
    { self, ... }@inputs:
    inputs.flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import inputs.nixpkgs {
          inherit system;
          overlays = [
            (final: prev: {
              nodejs-16_x = final.nodejs; # for npmlock2nix.v2
              npmlock2nix = pkgs.callPackage inputs.npmlock2nix { inherit pkgs; };
            })
            inputs.mvn2nix.overlay
          ];
        };
        pkgs-maven = import inputs.nixpkgs-maven { inherit system; };
        pkgs-ghostscript = import inputs.nixpkgs-ghostscript { inherit system; };
        #
        jdk = pkgs.jdk17;
        jre = pkgs.temurin-jre-bin-17;
        maven = pkgs-maven.maven.override { inherit jdk; };
        call-name = "vasara-pdf-tools-api";
        #
        clientPackagesSource = builtins.filterSource (
          path: type: (baseNameOf path) == "package.json" || (baseNameOf path) == "package-lock.json"
        ) ./camunda/client;
        client-call-name = "vasara-pdf-tools-client";
      in
      {
        apps.default = {
          type = "app";
          program = self.packages.${system}.default + "/bin/${call-name}";
        };

        packages.default =
          let
            fontsConf = pkgs.makeFontsConf {
              fontDirectories = [
                pkgs.carlito
                pkgs.dejavu_fonts
                pkgs.freefont_ttf
                pkgs.google-fonts
                pkgs.liberation_ttf_v2
                pkgs.noto-fonts
              ];
            };
            icc_profile = ./api/src/main/resources/AdobeRGB1998.icc;
            converter = ./api/src/main/resources/converter;
            jar = self.packages.${system}.jar;
          in
          pkgs.stdenv.mkDerivation rec {
            name = call-name;
            buildInputs = [ pkgs.makeWrapper ];
            propagatedBuildInputs = [
              pkgs.bash
              pkgs.coreutils
              pkgs.gawk
              pkgs-ghostscript.ghostscript
              pkgs.glibc.bin
              pkgs.gnugrep
              pkgs.gnused
              #
              pkgs.carlito
              pkgs.dejavu_fonts
              pkgs.freefont_ttf
              pkgs.google-fonts
              pkgs.liberation_ttf_v2
              pkgs.noto-fonts
            ];
            phases = [
              "installPhase"
              "fixupPhase"
            ];
            installPhase = ''
              mkdir -p $out/bin $out/var/lib $out/etc/fonts
              cp ${converter} $out/bin/converter
              cp ${icc_profile} $out/var/lib/AdobeRGB1998.icc
              cp ${fontsConf} $out/etc/fonts/fonts.conf
              cp ${jar} $out/var/lib/${call-name}.jar
              cat << EOF > $out/bin/${call-name}
              #!/usr/bin/env bash
              exec ${jre}/bin/java \$JAVA_OPTS \$@ -jar $out/var/lib/${call-name}.jar
              EOF
              chmod u+x $out/bin/${call-name}
            '';
            postFixup = ''
              wrapProgram $out/bin/converter \
                --prefix PATH : ${pkgs.lib.makeBinPath propagatedBuildInputs} \
                --set FONTMAP_GS_PATH "$(ls ${pkgs-ghostscript.ghostscript}/share/ghostscript/*/Resource/Init/Fontmap.GS)" \
                --set GS_FONTPATH "${pkgs.carlito}:${pkgs.dejavu_fonts}:${pkgs.freefont_ttf}:${pkgs.google-fonts}:${pkgs.liberation_ttf_v2}:${pkgs.noto-fonts}"
              wrapProgram $out/bin/${call-name} \
                --prefix PATH : ${pkgs.lib.makeBinPath propagatedBuildInputs} \
                --set SHELL "${pkgs.bash}/bin/bash" \
                --set CONVERTER "$out/bin/converter" \
                --set ICC_PROFILE "$out/var/lib/AdobeRGB1998.icc" \
                --set FONTMAP_GS_PATH "$(ls ${pkgs-ghostscript.ghostscript}/share/ghostscript/*/Resource/Init/Fontmap.GS)" \
                --set GS_FONTPATH "${pkgs.carlito}:${pkgs.dejavu_fonts}:${pkgs.freefont_ttf}:${pkgs.google-fonts}:${pkgs.liberation_ttf_v2}:${pkgs.noto-fonts}" \
                --set FALLBACK_FONTS "${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf:${pkgs.noto-fonts}/share/fonts/truetype/noto/NotoSansMath-Regular.ttf"
            '';
          };

        # Jar
        packages.jar =
          let
            mavenRepository = pkgs.buildMavenRepositoryFromLockFile { file = ./mvn2nix-lock.json; };
          in
          pkgs.stdenv.mkDerivation rec {
            pname = "api";
            version = "0.1.0-SNAPSHOT";
            name = "${pname}-${version}.jar";
            src = inputs.gitignore.lib.gitignoreSource ./.;
            buildInputs = [
              pkgs.bash
              pkgs.coreutils
              pkgs.gawk
              pkgs-ghostscript.ghostscript
              pkgs.glibc.bin
              pkgs.gnugrep
              pkgs.gnused
              jdk
              maven
            ];
            buildPhase = ''
              find . -print0|xargs -0 touch
              echo "mvn package --offline -Dmaven.repo.local=${mavenRepository}"
              patchShebangs --build ./api/src/main/resources/converter
              ICC_PROFILE="$(pwd)/api/src/main/resources/AdobeRGB1998.icc" \
              CONVERTER="$(pwd)/api/src/main/resources/converter" \
              mvn package --offline -Dmaven.repo.local=${mavenRepository}
            '';
            installPhase = ''
              mv api/target/${name} $out
              jar i $out
            '';
          };

        packages.image = pkgs.dockerTools.streamLayeredImage {
          name = "vasara-bpm/pdf-tools-api/${call-name}";
          tag = "latest";
          created = "now";
          maxLayers = 2;
          contents = [
            (pkgs.buildEnv {
              name = "image-contents";
              paths = [
                pkgs.busybox
                pkgs.curl
                (pkgs.dockerTools.fakeNss.override {
                  extraPasswdLines = [ "container:x:65:65:Container user:/var/empty:/bin/sh" ];
                  extraGroupLines = [ "container:!:65:" ];
                })
                pkgs.dockerTools.usrBinEnv
                pkgs.tini
                self.packages.${system}.default
              ];
              pathsToLink = [
                "/etc"
                "/sbin"
                "/bin"
              ];
            })
          ];
          extraCommands = ''
            mkdir -p usr/bin && ln -s /sbin/env usr/bin/env
            mkdir -p tmp && chmod a+rxwt tmp
          '';
          config = {
            Entrypoint = [
              "${pkgs.tini}/bin/tini"
              "--"
              "${self.packages.${system}.default}/bin/${call-name}"
            ];
            Env = [
              "TMPDIR=/tmp"
              "HOME=/tmp"
            ];
            Labels = { };
            User = "nobody"; # also container:65:65 supported
          };
        };

        packages.client_node_modules_dev = pkgs.npmlock2nix.v2.node_modules { src = clientPackagesSource; };

        packages.client_node_modules = pkgs.npmlock2nix.v2.node_modules {
          src = clientPackagesSource;
          postBuild = ''
            cp -f ${clientPackagesSource}/*.json .
            chmod a+rw *.json
            npm prune --omit=dev
          '';
        };

        packages.client = pkgs.stdenv.mkDerivation rec {
          name = call-name;
          src = inputs.gitignore.lib.gitignoreSource ./camunda/client;
          buildPhase = ''
            source $stdenv/setup;
            cp -a ${self.packages.${system}.client_node_modules_dev}/node_modules .
            NODE_ENV=production node_modules/.bin/tsc
          '';
          installPhase = ''
            source $stdenv/setup;
            mkdir -p $out/bin $out/var/lib/${client-call-name}
            cp -a dist/* $out/var/lib/${client-call-name}
            cat > $out/bin/${client-call-name} << EOF
            #!/usr/bin/env sh
            cd $out/var/lib/${client-call-name}
            exec node .
            EOF
            chmod u+x $out/bin/${client-call-name}
          '';
          postFixup = ''
            wrapProgram $out/bin/${client-call-name} \
              --prefix PATH : ${pkgs.lib.makeBinPath propagatedBuildInputs} \
              --suffix NODE_ENV : production \
              --suffix NODE_PATH : ${self.packages.${system}.client_node_modules}/node_modules
          '';
          buildInputs = [ pkgs.makeWrapper ];
          propagatedBuildInputs = [ pkgs.nodejs ];
        };

        packages.client-image = pkgs.dockerTools.streamLayeredImage {
          name = "vasara-bpm/pdf-tools-api/${client-call-name}";
          tag = "latest";
          created = "now";
          maxLayers = 2;
          contents = [
            (pkgs.buildEnv {
              name = "image-contents";
              paths = [
                pkgs.busybox
                pkgs.curl
                pkgs.gitMinimal
                (pkgs.dockerTools.fakeNss.override {
                  extraPasswdLines = [ "container:x:65:65:Container user:/var/empty:/bin/sh" ];
                  extraGroupLines = [ "container:!:65:" ];
                })
                pkgs.dockerTools.usrBinEnv
                pkgs.tini
                self.packages.${system}.client
              ];
              pathsToLink = [
                "/etc"
                "/sbin"
                "/bin"
              ];
            })
          ];
          extraCommands = ''
            mkdir -p usr/bin && ln -s /sbin/env usr/bin/env
            mkdir -p tmp && chmod a+rxwt tmp
          '';
          config = {
            Entrypoint = [
              "${pkgs.tini}/bin/tini"
              "--"
              "${self.packages.${system}.client}/bin/${client-call-name}"
            ];
            Env = [
              "TMPDIR=/tmp"
              "HOME=/tmp"
            ];
            Labels = { };
            User = "nobody"; # also container:65:65 supported
          };
        };

        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs.cachix
            pkgs.entr
            pkgs.openssl
            pkgs.jq
            #
            pkgs.bash
            pkgs.coreutils
            pkgs.gawk
            pkgs-ghostscript.ghostscript
            pkgs.glibc.bin
            pkgs.gnugrep
            pkgs.gnused
            jdk
            maven
            inputs.mvn2nix.defaultPackage.${system}
          ];
          shellHook = ''
            export SHELL=${pkgs.bash}/bin/bash
            export CONVERTER="$(pwd)/api/src/main/resources/converter"
            export ICC_PROFILE="$(pwd)/api/src/main/resources/AdobeRGB1998.icc"
            export GS_FONTPATH="${pkgs.google-fonts}/share/fonts/truetype"
            export FALLBACK_FONTS="${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf:${pkgs.noto-fonts}/share/fonts/truetype/noto/NotoSansMath-Regular.otf"
            echo CONVERTER="$(pwd)/api/src/main/resources/converter"
            echo ICC_PROFILE="$(pwd)/api/src/main/resources/AdobeRGB1998.icc"
            echo GS_FONTPATH="${pkgs.google-fonts}/share/fonts/truetype"
            echo FALLBACK_FONTS="${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf:${pkgs.noto-fonts}/share/fonts/truetype/noto/NotoSansMath-Regular.Otf"
          '';
        };

        devShells.client = pkgs.mkShell {
          buildInputs = [
            pkgs.cachix
            pkgs.entr
            pkgs.openssl
            pkgs.nodejs
            pkgs.jq
          ];
        };

        devShells.with-podman = inputs.vasara-bpm.devShells.${system}.podman.overrideAttrs (old: {
          buildInputs = old.buildInputs ++ self.devShells.${system}.default.buildInputs;
        });

        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
