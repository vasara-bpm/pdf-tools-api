# pdf-tools-api
OpenAPI REST service for filling PDF forms and converting PDF files to PDF/A.

## Usage
### Prerequisites
* [Docker](https://www.docker.com/get-started#h_installation)

### Starting
The application runs on a port **8080** by default.

Follow these steps to build Docker image and run an application:

1. Clone this repository

        git clone git@gitlab.com:vasara-bpm/pdf-tools-api.git

2. Build the image

        cd pdf-tools-api
        docker build -t pdf-tools .
        
3. Start an app

        docker run -p 8080:8080 pdf-tools
        
### OpenAPI and Swagger UI
Interaction with API's resources is available at:

        http://localhost:8080/swagger-ui.html

OpenAPI description is available at:
        
        http://localhost:8080/v3/api-docs/v1
        
### Additional parameters
The application properties can be set up using environment variables.

| Property | Description | Default |
| --- | ---| --- |
| MAX_FILE_SIZE | Maximum upload file size (MB). | 3 |
| CONVERTER | Absolute path to bash script. The path to original script is `../api/src/main/resources/converter` | In Dockerfile (`/pdftools`) |
| ICC_PROFILE | Absolute path to ICC profile. The path to original file is `../api/src/main/resources/AdobeRGB1998.icc` | In Dockerfile (`/pdftools`) | 