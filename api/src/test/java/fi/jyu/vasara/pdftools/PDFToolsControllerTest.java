package fi.jyu.vasara.pdftools;

import fi.jyu.vasara.pdftools.service.PDFToolsService;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.verapdf.pdfa.VeraGreenfieldFoundryProvider;

import java.time.Duration;

@WebFluxTest
@ExtendWith(SpringExtension.class)
@Import(PDFToolsService.class)
@Log4j2
public class PDFToolsControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @BeforeAll
    public static void init() {
        VeraGreenfieldFoundryProvider.initialise();
        log.info("VeraGreenfieldFoundryProvider initialized");
    }

    @BeforeEach
    public void setUp() {
        webTestClient = webTestClient
                .mutate()
                .responseTimeout(Duration.ofMillis(1000 * 60 * 60))  // long timeout to allow interactive debugging
                .build();
    }

    @Test
    public void testSuccessConvert() {
        MultiValueMap<String, HttpEntity<?>> data = getMultipartBody("PdfFormExample.pdf");
        webTestClient
                .post()
                .uri("/api/v1/convert")
                .accept(MediaType.APPLICATION_PDF)
                .body(BodyInserters.fromMultipartData(data))
                .exchange()
                .expectStatus()
                .isOk();
    }


    private MultiValueMap<String, HttpEntity<?>> getMultipartBody(String pdfFile) {
        MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
        multipartBodyBuilder.part("pdf", new ClassPathResource(pdfFile), MediaType.APPLICATION_PDF);

        return multipartBodyBuilder.build();
    }
}
