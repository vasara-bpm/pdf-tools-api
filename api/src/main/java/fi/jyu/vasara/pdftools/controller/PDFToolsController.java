package fi.jyu.vasara.pdftools.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.jyu.vasara.pdftools.exception.EmptyResponseException;
import fi.jyu.vasara.pdftools.service.PDFToolsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.codec.multipart.FormFieldPart;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.UnsupportedMediaTypeException;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.io.UnsupportedEncodingException;
import java.util.*;

@Log4j2
@RestController
@RequestMapping("api/v1")
public class PDFToolsController {
    private final PDFToolsService pdfToolsService;

    public PDFToolsController(PDFToolsService pdfToolsService) {
        this.pdfToolsService = pdfToolsService;
    }

    @PostMapping(
            value = "/convert",
            produces = MediaType.APPLICATION_PDF_VALUE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE
    )
    @Operation(
            summary = "Upload pdf file",
            description = "Upload pdf file to convert into PDF/A-2b format",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "The uploaded pdf file is already valid or converted into PDF/A format successfully.",
                            content = @Content(mediaType = MediaType.APPLICATION_PDF_VALUE)
                    ),
                    @ApiResponse(responseCode = "500", description = "We messed up. Please let us know so we can fix it ASAP.", content = @Content)
            })
    public Mono<ResponseEntity<byte[]>> convert(
            @Parameter(description = "Max file size: ${spring.servlet.multipart.max-file-size} Mb")
            @RequestPart(value = "pdf")
                    Mono<FilePart> filePart
    ) {
        return filePart
                .doOnNext(fp -> log.info("File name <{}>", fp.filename()))
                .flatMap(pdfFile -> {
                    if (!Objects.requireNonNull(pdfFile.headers().getContentType()).equals(MediaType.APPLICATION_PDF)) {
                        return Mono.error(new UnsupportedMediaTypeException("Unsupported media type."));
                    }

                    return Mono
                            .fromCallable(() -> pdfToolsService.convert(pdfFile))
                            .subscribeOn(Schedulers.boundedElastic())
                            .flatMap(result -> {
                                if (result.getStatusCode() == HttpStatus.NO_CONTENT) {
                                    return Mono.error(new EmptyResponseException("Sorry, a technical error occurred."));
                                } else if (result.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
                                    byte[] body = result.getBody();
                                    try {
                                        if (body != null) {
                                            return Mono.error(new EmptyResponseException(new String(body, "UTF-8")));
                                        }
                                    } catch (UnsupportedEncodingException e) {
                                        //
                                    }
                                    return Mono.error(new EmptyResponseException("Sorry, a technical error occurred."));
                                }
                                return Mono.just(result);
                            });
                });
    }

    @PostMapping(
            value = "/extract",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE
    )
    @Operation(
            summary = "Get pdf form structure.",
            description = "Upload pdf file to get form fields structure",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "The forms structure of pdf file parsed successfully.",
                            content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE)
                    ),
                    @ApiResponse(responseCode = "500", description = "We messed up. Please let us know so we can fix it ASAP.", content = @Content)
            })
    public Mono<ResponseEntity<String>> extract(
            @Parameter(description = "Max file size: ${spring.servlet.multipart.max-file-size} Mb")
            @RequestPart(value = "pdf")
                    Mono<FilePart> filePart
    ) {
        return filePart
                .doOnNext(fp -> log.info("File name <{}>", fp.filename()))
                .flatMap(pdfFile -> {
                    if (!Objects.requireNonNull(pdfFile.headers().getContentType()).equals(MediaType.APPLICATION_PDF)) {
                        return Mono.error(new UnsupportedMediaTypeException("Unsupported media type."));
                    }

                    return Mono
                            .fromCallable(() -> pdfToolsService.extract(pdfFile))
                            .subscribeOn(Schedulers.boundedElastic())
                            .flatMap(result -> {
                                if (result.getStatusCode() == HttpStatus.NO_CONTENT) {
                                    return Mono.error(new EmptyResponseException("Sorry, a technical error occurred."));
                                }
                                return Mono.just(result);
                            });
                });
    }

    @PostMapping(
            value = "/fill",
            produces = MediaType.APPLICATION_PDF_VALUE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE
    )
    @Operation(
            summary = "Fill pdf form structure.",
            description = "Upload pdf file and form fields map to get filled form",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "PDF with form fields filled.",
                            content = @Content(mediaType = MediaType.APPLICATION_PDF_VALUE)
                    ),
                    @ApiResponse(responseCode = "500", description = "We messed up. Please let us know so we can fix it ASAP.", content = @Content)
            })

    public Mono<ResponseEntity<byte[]>> fill(
            @Parameter(description = "Max file size: ${spring.servlet.multipart.max-file-size} Mb")
            @RequestPart(value = "pdf") Mono<FilePart> filePart,
            @RequestPart(value = "fields") Mono<FormFieldPart> fieldsPart
    ) {
        return Mono.zip(filePart, fieldsPart).flatMap( r -> {
            FilePart pdfFile = r.getT1();
            FormFieldPart fields = r.getT2();
            ObjectMapper fieldsMapper = new ObjectMapper();
            Map<String, Object> optionsMap = new TreeMap<>();
            Map<String, Object> fieldsMap = null;
            List<Map<String, Object>> fieldsMapArray = null;
            if (!Objects.requireNonNull(pdfFile.headers().getContentType()).equals(MediaType.APPLICATION_PDF)) {
                return Mono.error(new UnsupportedMediaTypeException("Unsupported media type."));
            }
            try {
                fieldsMap = fieldsMapper.readValue(fields.value(), new TypeReference<>() {});
            } catch (JsonProcessingException e) {
                try {
                    fieldsMapArray = fieldsMapper.readValue(fields.value(), new TypeReference<>() {});
                } catch (JsonProcessingException e2) {
                    return Mono.error(new EmptyResponseException(e.toString()));
                }
            }
            List<Map<String, Object>> fieldsMapArrayFinal = fieldsMapArray != null ? fieldsMapArray : new ArrayList<>(Arrays.asList(fieldsMap));
            return Mono
                    .fromCallable(() -> pdfToolsService.fill(pdfFile, fieldsMapArrayFinal, optionsMap))
                    .subscribeOn(Schedulers.boundedElastic())
                    .flatMap(result -> {
                        if (result.getStatusCode() == HttpStatus.NO_CONTENT) {
                            return Mono.error(new EmptyResponseException("Sorry, a technical error occurred."));
                        }
                        return Mono.just(result);
                    });
        }).switchIfEmpty(Mono.zip(filePart, fieldsPart).flatMap( r -> {
            FilePart pdfFile = r.getT1();
            FormFieldPart fields = r.getT2();
            ObjectMapper fieldsMapper = new ObjectMapper();
            Map<String, Object> optionsMap = new TreeMap<>();
            Map<String, Object> fieldsMap = null;
            List<Map<String, Object>> fieldsMapArray = null;
            if (!Objects.requireNonNull(pdfFile.headers().getContentType()).equals(MediaType.APPLICATION_PDF)) {
                return Mono.error(new UnsupportedMediaTypeException("Unsupported media type."));
            }
            try {
                fieldsMap = fieldsMapper.readValue(fields.value(), new TypeReference<>() {});
            } catch (JsonProcessingException e) {
                try {
                    fieldsMapArray = fieldsMapper.readValue(fields.value(), new TypeReference<>() {});
                } catch (JsonProcessingException e2) {
                    return Mono.error(new EmptyResponseException(e.toString()));
                }
            }
            List<Map<String, Object>> fieldsMapArrayFinal = fieldsMapArray != null ? fieldsMapArray : new ArrayList<>(Arrays.asList(fieldsMap));
            return Mono
                    .fromCallable(() -> pdfToolsService.fill(pdfFile, fieldsMapArrayFinal, optionsMap))
                    .subscribeOn(Schedulers.boundedElastic())
                    .flatMap(result -> {
                        if (result.getStatusCode() == HttpStatus.NO_CONTENT) {
                            return Mono.error(new EmptyResponseException("Sorry, a technical error occurred."));
                        }
                        return Mono.just(result);
                    });


        }));
    }
}

