package fi.jyu.vasara.pdftools.handler;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.reactive.function.UnsupportedMediaTypeException;
import fi.jyu.vasara.pdftools.exception.EmptyResponseException;
import reactor.core.publisher.Mono;

@Log4j2
@RestControllerAdvice
public class PDFToolsControllerExceptionHandler {

    @ExceptionHandler(value = UnsupportedMediaTypeException.class)
    Mono<ResponseEntity> unsupportedMediaTypeHandler(UnsupportedMediaTypeException e) {
        return getMono(HttpStatus.UNSUPPORTED_MEDIA_TYPE, e);
    }

    @ExceptionHandler(value = EmptyResponseException.class)
    Mono<ResponseEntity> emptyResponseExceptionHandler(EmptyResponseException e) {
        return getMono(HttpStatus.INTERNAL_SERVER_ERROR, e);
    }

    private Mono<ResponseEntity> getMono(HttpStatus httpStatus, Throwable e) {
        log.error(e.getMessage(), e);

        return Mono
                .just(ResponseEntity
                        .status(httpStatus)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(new GlobalErrorHandler
                                .HttpError(e.getMessage())));
    }
}
