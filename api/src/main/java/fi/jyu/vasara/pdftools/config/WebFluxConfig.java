package fi.jyu.vasara.pdftools.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.http.codec.multipart.DefaultPartHttpMessageReader;
import org.springframework.http.codec.multipart.MultipartHttpMessageReader;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@Configuration
@EnableWebFlux
public class WebFluxConfig implements WebFluxConfigurer {

    @Value("${spring.servlet.multipart.max-file-size}")
    private String maxFileSize;

    @Override
    public void configureHttpMessageCodecs(ServerCodecConfigurer configurer) {
        DefaultPartHttpMessageReader partReader = new DefaultPartHttpMessageReader();
        partReader.setMaxParts(3);
        partReader.setMaxDiskUsagePerPart(Long.parseLong(maxFileSize) * 1024 * 1024); // in MB
        MultipartHttpMessageReader multipartReader = new MultipartHttpMessageReader(partReader);
        configurer.defaultCodecs().maxInMemorySize(Integer.parseInt(maxFileSize) * 1024 * 1024); // in MB
        configurer.defaultCodecs().multipartReader(multipartReader);
    }

}
