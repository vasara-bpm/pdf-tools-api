package fi.jyu.vasara.pdftools;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(
                title = "PDF Tools API",
                version = "1.0",
                description = "OpenAPI REST service for filling PDF forms and converting PDF files to PDF/A",
            contact = @Contact(
                name = "JYU",
                url = "https://www.jyu.fi/digipalvelut/"
            ),
            license = @License(
                name = "EUPL",
                url = "https://gitlab.com/vasara-bpm/pdf-tools-api/blob/master/LICENSE"
            )
        )
)
public class PDFToolsApplication {

    public static void main(String[] args) {
        SpringApplication.run(PDFToolsApplication.class, args);
    }

    @Bean
    public GroupedOpenApi v1OpenApi() {
        String[] paths = {"/api/v1/**"};
        return GroupedOpenApi.builder().group("v1").pathsToMatch(paths).build();
    }
}
