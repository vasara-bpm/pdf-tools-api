package fi.jyu.vasara.pdftools.service;

import com.google.zxing.WriterException;
import fi.jyu.vasara.pdftools.utils.PDFToolsUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.fontbox.ttf.OTFParser;
import org.apache.fontbox.ttf.OpenTypeFont;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSInteger;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.*;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotation;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.form.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import org.vandeseer.easytable.TableDrawer;
import org.vandeseer.easytable.settings.HorizontalAlignment;
import org.vandeseer.easytable.structure.Row;
import org.vandeseer.easytable.structure.Table;
import org.vandeseer.easytable.structure.cell.TextCell;
import org.verapdf.core.EncryptedPdfException;
import org.verapdf.core.ModelParsingException;
import org.verapdf.core.ValidationException;
import org.verapdf.metadata.fixer.gf.GFMetadataFixerImpl;
import org.verapdf.metadata.fixer.utils.ValidationStatus;
import org.verapdf.pdfa.Foundries;
import org.verapdf.pdfa.MetadataFixer;
import org.verapdf.pdfa.PDFAParser;
import org.verapdf.pdfa.results.MetadataFixerResult;
import org.verapdf.pdfa.results.ValidationResult;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static fi.jyu.vasara.pdftools.utils.QRCodePDFGenerator.createQRCodeImage;

@Log4j2
@Service
public class PDFToolsService implements PDFTools {
    @Value("${converter.retries}")
    private Integer retries;
    @Value("${converter.shell}")
    private String shell;
    @Value("${converter.script}")
    private String script;
    @Value("${converter.iccprofile}")
    private String iccProfile;
    @Value("${converter.fallbackfonts}")
    private String fallbackFonts;

    @Override
    public ResponseEntity<byte[]> convert(FilePart pdfFile) {
        String filenameWithoutExtension = pdfFile.filename().substring(0, FilenameUtils.indexOfExtension(pdfFile.filename()));
        PDFAParser parser = null;
        Path tempDir = null;
        File inputFile = null;
        File outputFile = null;
        StringBuilder errorMessage = new StringBuilder();

        try {
            InputStream is = PDFToolsUtils.getInputStreamFromFlux(pdfFile.content());
            // Parser will generate temp pdf file in OS temp path.
            // InputStream parameter will be automatically closed when parser is created.
            parser = Foundries.defaultInstance().createParser(is);

            // Note: VeraPDF 0.19.x would expose path to its InternalInputStream,
            // not sure if re-using tmp file from other library is worth of the risks.
            // We reverted to VeraPDF 0.18.x to use be able to use released versions.

            tempDir = Files.createTempDirectory("tmpPDF");
            inputFile = new File(tempDir.toUri().getPath() + "input.pdf");
            // Note: transferTo requires >= Java 13
            PDFToolsUtils.getInputStreamFromFlux(pdfFile.content()).transferTo(new FileOutputStream(inputFile, false));
            outputFile = new File(tempDir.toUri().getPath() + "output.pdf");

            ValidationResult validationResult = PDFToolsUtils.getValidationResult(parser);
            int standardPart = validationResult.getPDFAFlavour().getPart().getPartNumber();
            String conformanceLevel = validationResult.getPDFAFlavour().getLevel().getCode();

            ValidationStatus validationStatus = PDFToolsUtils.getValidationStatus(validationResult);

            switch (validationStatus) {
                // Return the original file
                case VALID: {
                    log.info("The uploaded document is valid");
                    // This is the original file we will push back to the end user
                    byte[] originalFile = IOUtils.toByteArray(PDFToolsUtils.getInputStreamFromFlux(pdfFile.content()));

                    return ResponseEntity
                            .ok()
                            .contentType(MediaType.APPLICATION_PDF)
                            .header(HttpHeaders.CONTENT_DISPOSITION,
                                    String.format("attachment; filename=%s_PDFA-%s.pdf", filenameWithoutExtension, standardPart + conformanceLevel))
                            .body(originalFile);
                }
                // Let's try to fix document's metadata
                case INVALID_METADATA: {
                    MetadataFixer metadataFixer = new GFMetadataFixerImpl();
                    ByteArrayOutputStream fixedMetadataOs = new ByteArrayOutputStream();
                    MetadataFixerResult metadataFixerResult = metadataFixer.fixMetadata(parser, fixedMetadataOs, validationResult);

                    if (metadataFixerResult.getRepairStatus() == MetadataFixerResult.RepairStatus.SUCCESS) {
                        log.info("The metadata fixed successfully: {}", metadataFixerResult.getAppliedFixes());
                        byte[] fixedMetadata = fixedMetadataOs.toByteArray();

                        return ResponseEntity
                                .ok()
                                .contentType(MediaType.APPLICATION_PDF)
                                .header(HttpHeaders.CONTENT_DISPOSITION,
                                        String.format("attachment; filename=%s_PDFA-%s.pdf", filenameWithoutExtension, standardPart + conformanceLevel))
                                .body(fixedMetadata);
                    } else {
                        log.error("Error while metadata fixing({}): {}",
                                metadataFixerResult.getRepairStatus(),
                                metadataFixerResult.getAppliedFixes()
                        );
                    }
                }
                // In another case convert file to PDF/A-2b
                default: {
                    for (int i = 0; i < retries; i = i + 1) {
                        if (outputFile.exists()) {
                            inputFile.delete();
                            outputFile.renameTo(inputFile);
                        }
                        var processBuilder = new ProcessBuilder(
                                shell,
                                script,
                                "--iccprofile=" + iccProfile,
                                "--cleanTemp=" + true,
                                inputFile.getAbsolutePath(),
                                outputFile.getAbsolutePath()
                        );

                        Process process = processBuilder.start();
                        process.waitFor();

                        // stderr
                        String stderrLine;
                        BufferedReader stderrReader =
                                new BufferedReader(new InputStreamReader(process.getErrorStream()));
                        StringBuilder stderrBuilder = new StringBuilder();
                        while ((stderrLine = stderrReader.readLine()) != null) {
                            stderrBuilder.append(stderrLine);
                            stderrBuilder.append(System.getProperty("line.separator"));
                        }
                        if (stderrBuilder.length() > 0) {
                            log.warn(stderrBuilder);
                            errorMessage.append(stderrBuilder);
                        }

                        // stdout
                        String stdoutLine;
                        BufferedReader stdoutReader =
                                new BufferedReader(new InputStreamReader(process.getInputStream()));
                        StringBuilder stdoutBuilder = new StringBuilder();
                        while ((stdoutLine = stdoutReader.readLine()) != null) {
                            stdoutBuilder.append(stdoutLine);
                            stdoutBuilder.append(System.getProperty("line.separator"));
                        }
                        if (stdoutBuilder.length() > 0) {
                            log.info(stdoutBuilder);
                            errorMessage.append(stdoutBuilder);
                        }

                        // Let's check if script's output is valid PDF/A-2b
                        parser = Foundries.defaultInstance().createParser(outputFile);
                        ValidationResult result = PDFToolsUtils.getValidationResult(parser);
                        ValidationStatus status = PDFToolsUtils.getValidationStatus(result);

                        if (status == ValidationStatus.VALID) {
                            return ResponseEntity
                                    .ok()
                                    .contentType(MediaType.APPLICATION_PDF)
                                    .header(HttpHeaders.CONTENT_DISPOSITION,
                                            String.format("attachment; filename=%s_PDFA-%s.pdf",
                                                    filenameWithoutExtension,
                                                    result.getPDFAFlavour().getPart().getPartNumber() + result.getPDFAFlavour().getLevel().getCode()
                                            )
                                    )
                                    .body(FileUtils.readFileToByteArray(outputFile));
                        } else if (i < retries - 1) {
                            errorMessage.append("The conversation result is still not valid PDF/A-2b (" + status + "). Retrying...");
                            log.error("The conversation result is still not valid PDF/A-2b ({}). Retrying...", status);
                        } else {
                            errorMessage.append("The conversation result is still not valid PDF/A-2b (" + status + "). Return nothing ...");
                            log.error("The conversation result is still not valid PDF/A-2b ({}). Return nothing ...", status);
                        }
                    }
                }
            }
        } catch (InterruptedException | ValidationException | ParserConfigurationException | IOException |
                 ModelParsingException | EncryptedPdfException | URISyntaxException | SAXException e) {
            log.error("PDFToolsService exception: ", e);
        } finally {
            if (inputFile != null) {
                inputFile.delete();
            }
            if (outputFile != null) {
                outputFile.delete();
            }
            try {
                if (parser != null) {
                    parser.close();
                }
                if (tempDir != null) {
                    Files.deleteIfExists(tempDir);
                }
            } catch (IOException e) {
                log.error(e.getStackTrace());
            }
        }
        if (errorMessage.length() > 0) {
            try {
                return ResponseEntity.status(500).body(errorMessage.toString().getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                return ResponseEntity.noContent().build();
            }
        } else {
            return ResponseEntity.noContent().build();
        }

    }

    @Override
    public ResponseEntity<String> extract(FilePart pdfFile) {
        try {
            org.apache.pdfbox.pdmodel.PDDocument pddDocument = PDDocument.load(PDFToolsUtils.getInputStreamFromFlux(pdfFile.content()));
            PDDocumentCatalog documentCatalog = pddDocument.getDocumentCatalog();
            PDAcroForm acroForm = documentCatalog.getAcroForm();

            if (acroForm != null) {
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(PDFToolsUtils.printFields(acroForm));
            } else {
                return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body("{}");
            }
        } catch (IOException e) {
            log.error(e.getStackTrace());
        }

        return ResponseEntity.noContent().build();
    }

    public static byte[] mergePdfBytes(List<byte[]> pdfBytes) throws IOException {
        PDFMergerUtility merger = new PDFMergerUtility();
        PDDocument resultDocument = new PDDocument();

        for (byte[] pdfByte : pdfBytes) {
            if (pdfByte == null) {
                throw new IOException("No content.");
            }
            try (ByteArrayInputStream inputStream = new ByteArrayInputStream(pdfByte);
                 PDDocument document = PDDocument.load(inputStream)) {
                merger.appendDocument(resultDocument, document);
            }
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        resultDocument.save(outputStream);
        resultDocument.close();

        return outputStream.toByteArray();
    }

    @Override
    public ResponseEntity<byte[]> fill(FilePart template, List<Map<String, Object>> data, Map<String, Object> options) {
        List<byte[]> pages = new ArrayList<>();
        for (Map<String, Object> data_ : data) {
            ArrayList<String> fallbackFontPaths = new ArrayList<>();
            boolean added = false;
            for (String path : this.fallbackFonts.split(":")) {
                try {
                    byte[] page = fillImpl(template, data_, options, fallbackFontPaths);
                    if (page != null) {
                        pages.add(fillImpl(template, data_, options, fallbackFontPaths));
                        added = true;
                        break;
                    }
                } catch (IllegalArgumentException e) {
                    // java.lang.IllegalArgumentException: ... is not available in this font's encoding: WinAnsiEncoding
                    fallbackFontPaths.add(path);  // add a fallback font, because template got unexpected characters
                }
            }
            if (!added) {
                byte[] page = fillImpl(template, data_, options, fallbackFontPaths);
                if (page != null) {
                    pages.add(fillImpl(template, data_, options, fallbackFontPaths));
                }
            }
        }
        if (pages.size() > 0) {
            try {
                return ResponseEntity
                        .ok()
                        .contentType(MediaType.APPLICATION_PDF)
                        .header(HttpHeaders.CONTENT_DISPOSITION,
                                String.format("attachment; filename=%s", template.filename())
                        ).body(pages.size() == 1 ? pages.get(0) : mergePdfBytes(pages));
            } catch (IOException e) {
                return ResponseEntity.noContent().build();
            }
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    private void setFieldValueImpl(PDField field, Object value, String fontName, Integer forcedFontSize) throws IOException {
        if (field instanceof PDTextField && ((PDTextField) field).isMultiline() && forcedFontSize == null) {
            forcedFontSize = 0;
        }
        if (field != null) {
            COSDictionary dict = field.getCOSObject();
            COSString defaultAppearance = (COSString) dict.getDictionaryObject(COSName.DA);
            if (defaultAppearance != null) {
                if (fontName.length() > 0 && forcedFontSize != null) {
                    // allow flexible font size and force font to a known good font
                    float fieldFontSize = extractFontSize(defaultAppearance);
                    dict.setString(COSName.DA, defaultAppearance.getASCII().replaceAll("[0-9]+ Tf", forcedFontSize + " Tf").replaceAll("/\\w+", "/" + fontName));
                    if (forcedFontSize == 0 && value instanceof String) {
                        // allow downwards flexible size
                        PDFontSizeCalculator calc = new PDFontSizeCalculator((PDVariableText) field, (String) value);
                        float calcFontSize = calc.calculateFontSize(field.getWidgets().get(0).getRectangle());
                        if (calcFontSize >= fieldFontSize && fieldFontSize > 0) {
                            dict.setString(COSName.DA, defaultAppearance.getASCII().replaceAll("[0-9]+ Tf", fieldFontSize+ " Tf").replaceAll("/\\w+", "/" + fontName));
                        }
                    }
                } else if (fontName.length() > 0) {
                    // force font to a known good font
                    dict.setString(COSName.DA, defaultAppearance.getASCII().replaceAll("/\\w+", "/" + fontName));
                } else if (forcedFontSize != null) {
                    // allow flexible size
                    float fieldFontSize = extractFontSize(defaultAppearance);
                    dict.setString(COSName.DA, defaultAppearance.getASCII().replaceAll("[0-9]+ Tf", forcedFontSize + " Tf"));
                    if (forcedFontSize == 0 && value instanceof String) {
                        // allow downwards flexible size
                        PDFontSizeCalculator calc = new PDFontSizeCalculator((PDVariableText) field, (String) value);
                        float calcFontSize = calc.calculateFontSize(field.getWidgets().get(0).getRectangle());
                        if (calcFontSize >= fieldFontSize && fieldFontSize > 0) {
                            dict.setString(COSName.DA, defaultAppearance.getASCII().replaceAll("[0-9]+ Tf", fieldFontSize + " Tf"));
                        }
                    }
                }
            } else if (fontName.length() > 0 && forcedFontSize != null) {
                String defaultAppearanceString = "/" + fontName + " " + forcedFontSize + " Tf 0 g";
                dict.setString(COSName.DA, defaultAppearanceString);
            } else if (fontName.length() > 0) {
                String defaultAppearanceString = "/" + fontName + " 12 Tf 0 g";
                dict.setString(COSName.DA, defaultAppearanceString);
            } else {
                String defaultAppearanceString = "/Helvetica 12 Tf 0 g";
                dict.setString(COSName.DA, defaultAppearanceString);
            }
        }
        if (field != null && value instanceof String) {
            // Remove control and known "bad" unicode characters
            field.setValue(((String) value).replaceAll("[\\u202C\\p{Cntrl}&&[^\r\n\t]]", ""));
        }
        if (field != null && value instanceof Double) {
            field.setValue(String.format("%.2f", value).replaceAll("\\.", ","));
        }
        if (field != null && value instanceof Integer) {
            field.setValue(String.valueOf(value));
        }
        if (field != null && value instanceof Boolean) {
            if (field instanceof PDCheckBox) {
                if ((Boolean) value) {
                    ((PDCheckBox) field).check();
                } else {
                    ((PDCheckBox) field).unCheck();
                }
            } else {
                // TODO: How to localize?
                field.setValue((Boolean) value ? "[ X ]" : "[    ]");
            }
        }
    }

    private void setFieldValue(PDField field, Object value, LinkedHashMap<String, PDFont> fallbackFonts, Integer forcedFontSize) throws IOException {
        try {
            setFieldValueImpl(field, value, "", null);
            return;
        } catch (IllegalArgumentException e) {
            for (String fontName : fallbackFonts.keySet()) {
                try {
                    setFieldValueImpl(field, value, fontName, forcedFontSize);
                    return;
                } catch (IllegalArgumentException e2) {
                    // java.lang.IllegalArgumentException: ... is not available in this font's encoding: WinAnsiEncoding
                    // try next fallback font, because template got unexpected characters
                }
            }
        }
        // All fonts failed and this will raise an exception...
        setFieldValueImpl(field, value, "", null);
    }

    private PDField removeField(PDDocument document, String fullFieldName) throws IOException {
        PDDocumentCatalog documentCatalog = document.getDocumentCatalog();
        PDAcroForm acroForm = documentCatalog.getAcroForm();

        if (acroForm == null) {
            System.out.println("No form defined.");
            return null;
        }

        PDField targetField = null;

        for (PDField field : acroForm.getFieldTree()) {
            if (fullFieldName.equals(field.getFullyQualifiedName())) {
                targetField = field;
                break;
            }
        }
        if (targetField == null) {
            System.out.println("Form does not contain field with given name.");
            return null;
        }

        PDNonTerminalField parentField = targetField.getParent();
        if (parentField != null) {
            List<PDField> childFields = parentField.getChildren();
            boolean removed = false;
            for (PDField field : childFields) {
                if (field.getCOSObject().equals(targetField.getCOSObject())) {
                    removed = childFields.remove(field);
                    parentField.setChildren(childFields);
                    break;
                }
            }
            if (!removed)
                System.out.println("Inconsistent form definition: Parent field does not reference the target field.");
        } else {
            List<PDField> rootFields = acroForm.getFields();
            boolean removed = false;
            for (PDField field : rootFields) {
                if (field.getCOSObject().equals(targetField.getCOSObject())) {
                    removed = rootFields.remove(field);
                    break;
                }
            }
            if (!removed)
                System.out.println("Inconsistent form definition: Root fields do not include the target field.");
        }

        removeWidgets(targetField);

        return targetField;
    }

    private void removeWidgets(PDField targetField) throws IOException {
        if (targetField instanceof PDTerminalField) {
            List<PDAnnotationWidget> widgets = ((PDTerminalField) targetField).getWidgets();
            for (PDAnnotationWidget widget : widgets) {
                PDPage page = widget.getPage();
                if (page != null) {
                    List<PDAnnotation> annotations = page.getAnnotations();
                    boolean removed = false;
                    for (PDAnnotation annotation : annotations) {
                        if (annotation.getCOSObject().equals(widget.getCOSObject())) {
                            removed = annotations.remove(annotation);
                            break;
                        }
                    }
                    if (!removed)
                        System.out.println("Inconsistent annotation definition: Page annotations do not include the target widget.");
                } else {
                    System.out.println("Widget annotation does not have an associated page; cannot remove widget.");
                    // TODO: In this case iterate all pages and try to find and remove widget in all of them
                }
            }
        } else if (targetField instanceof PDNonTerminalField) {
            List<PDField> childFields = ((PDNonTerminalField) targetField).getChildren();
            for (PDField field : childFields)
                removeWidgets(field);
        } else {
            System.out.println("Target field is neither terminal nor non-terminal; cannot remove widgets.");
        }
    }

    private void fillPageNumber(PDDocument document, float x, float y, PDFont font, Integer fontSize) throws IOException {
        int totalPages = document.getNumberOfPages();
        for (int i = 0; i < totalPages; i++) {
            PDPage page = document.getPage(i);
            PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true, true);
            contentStream.beginText();
            contentStream.setFont(font, fontSize);
            contentStream.newLineAtOffset(x, y);
            contentStream.showText(i + 1 + " (" + totalPages + ")");
            contentStream.endText();
            contentStream.close();
        }
    }

    private void fillTable(PDDocument document, PDAcroForm form, String key, Object value,
                           LinkedHashMap<String, PDFont> fallbackFonts, Integer forcedFontSize) throws IOException {
        // Use the first row as sample for mapping columns
        Object sampleItem = ((ArrayList) value).get(0);
        Set<String> keys = ((LinkedHashMap) sampleItem).keySet();

        float rowHeight = (float) 0;
        float pageHeight = (float) 0;

        // Sample field will be used to test if we need to use fallback fonts
        PDField sample = null;

        // Get and sort columns
        LinkedHashMap<String, PDField> columnMap = new LinkedHashMap<>();
        for (String key_ : keys) {
            for (PDField field : form.getFieldTree()) {
                if (field.getFullyQualifiedName().equals(key + "." + key_) || field.getFullyQualifiedName().startsWith(key + "." + key_ + "_")) {
                    if (!columnMap.containsKey(key_)) {
                        columnMap.put(key_, field);
                    } else {
                        PDRectangle previous = columnMap.get(key_).getWidgets().get(0).getRectangle();
                        PDRectangle next = field.getWidgets().get(0).getRectangle();
                        float diff = previous.getLowerLeftY() - next.getLowerLeftY();
                        if (diff < 0) {
                            columnMap.put(key_, field);
                        }
                        if (rowHeight == 0 || Math.abs(diff) < rowHeight) {
                            rowHeight = Math.abs(diff);
                        }
                        if (pageHeight == 0 || pageHeight > next.getLowerLeftY()) {
                            pageHeight = next.getLowerLeftY();
                        }
                    }
                    // Leave one field as sample for validating input against available fonts
                    if (sample == null && !columnMap.containsValue(field)) {
                        sample = field;
                    } else {
                        removeField(document, field.getFullyQualifiedName());
                    }
                }
            }
        }
        pageHeight = pageHeight - rowHeight / 2;
        ArrayList<PDField> columns = new ArrayList<>(columnMap.values());
        columns.sort((field1, field2) -> {
            PDRectangle rect1 = field1.getWidgets().get(0).getRectangle();
            PDRectangle rect2 = field2.getWidgets().get(0).getRectangle();
            if (rect1.getLowerLeftY() < rect2.getLowerLeftY()) {
                return -1;
            } else if (rect1.getLowerLeftY() > rect2.getLowerLeftY()) {
                return 1;
            } else {
                if (rect1.getLowerLeftX() < rect2.getLowerLeftX()) {
                    return -1;
                } else if (rect1.getLowerLeftX() > rect2.getLowerLeftX()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        // Calculate column widths
        float[] widths = new float[columns.size()];
        for (PDField field : columns) {
            int idx = columns.indexOf(field);
            PDAnnotationWidget widget = field.getWidgets().get(0);
            if (widget != null) {
                PDRectangle rect = widget.getRectangle();
                widths[idx] = rect.getWidth();
                if (idx > 0) {
                    widths[idx - 1] += rect.getLowerLeftX() - columns.get(idx - 1).getWidgets().get(0).getRectangle().getUpperRightX();
                }
            }
        }

        // Create a new blank page
        PDPage originalPage = columns.get(0).getWidgets().get(0).getPage();

        // Draw the original page's contents onto the new page
        PDPageContentStream originalContentStream = new PDPageContentStream(document, originalPage, PDPageContentStream.AppendMode.APPEND, true);

        // Get embedded fonts on the page
        LinkedHashMap<String, PDFont> fontMap = new LinkedHashMap<>();
        for (COSName fontName : originalPage.getResources().getFontNames()) {
            PDFont font = originalPage.getResources().getFont(fontName);
            fontMap.put(fontName.getName(), font);
        }

        // Build table
        Table.TableBuilder table = Table.builder().addColumnsOfWidth(widths);
        for (LinkedHashMap item : (ArrayList<LinkedHashMap<String, String>>) value) {
            Row.RowBuilder row = Row.builder();
            row.height(rowHeight);
            for (PDField field : columns) {
                Object value_ = item.get(field.getPartialName());
                TextCell.TextCellBuilder<?, ?> cell = TextCell.builder();
                if (value_ instanceof String) {
                    // Remove control and known "bad" unicode characters
                    cell.text(((String) value_).replaceAll("[\\u202C\\p{Cntrl}&&[^\r\n\t]]", ""));
                } else if (value_ instanceof Double) {
                    cell.text(String.format("%.2f", value_).replaceAll("\\.", ","));
                } else if (value_ instanceof Integer) {
                    cell.text(String.valueOf(value));
                } else if (value_ instanceof Boolean) {
                    cell.text((Boolean) value_ ? "X" : "");
                }
                cell.paddingRight(10);
                COSDictionary dict = field.getCOSObject();
                COSString defaultAppearance = (COSString) dict.getDictionaryObject(COSName.DA);
                COSInteger alignment = (COSInteger) dict.getDictionaryObject(COSName.Q);
                Pattern pattern = Pattern.compile("/([A-Za-z]+) ([0-9]+) Tf");
                Matcher matcher = pattern.matcher(defaultAppearance.toString());
                String fontName = "He";
                if (alignment != null && alignment.intValue() == 0) {
                    cell.horizontalAlignment(HorizontalAlignment.LEFT);
                } else if (alignment != null && alignment.intValue() == 1) {
                    cell.horizontalAlignment(HorizontalAlignment.CENTER);
                } else if (alignment != null && alignment.intValue() == 2) {
                    cell.horizontalAlignment(HorizontalAlignment.RIGHT);
                }
                if (matcher.find()) {
                    if (forcedFontSize != null) {
                        cell.fontSize(forcedFontSize);
                    } else {
                        int fontSize = Integer.parseInt(matcher.group(2));
                        if (fontSize > 0) {
                            cell.fontSize(fontSize);
                        }
                    }
                    fontName = matcher.group(1);
                }
                try {
                    if (fontMap.containsKey(fontName)) {
                        cell.font(fontMap.get(fontName));
                        setFieldValueImpl(sample, value_, fontName, null);
                    } else {
                        cell.font(PDType1Font.HELVETICA);
                        setFieldValueImpl(sample, value_, "Helvetica", null);
                    }
                } catch (IllegalArgumentException e) {
                    int counter = fallbackFonts.size();
                    for (String fontName_ : fallbackFonts.keySet()) {
                        try {
                            cell.font(fallbackFonts.get(fontName_));
                            setFieldValueImpl(sample, value_, fontName_, null);
                            break;
                        } catch (IllegalArgumentException e2) {
                            counter -= 1;
                            if (counter == 0) {
                                throw e2;
                            }
                            // java.lang.IllegalArgumentException: ... is not available in this font's encoding: WinAnsiEncoding
                            // try next fallback font, because template got unexpected characters
                        }
                    }
                }
                row.add(cell.build());
            }
            table.addRow(row.build());
        }

        // Set up the drawer
        TableDrawer tableDrawer = TableDrawer.builder()
                .startX(columns.get(0).getWidgets().get(0).getRectangle().getLowerLeftX())
                .startY(columns.get(0).getWidgets().get(0).getRectangle().getLowerLeftY() +
                        columns.get(0).getWidgets().get(0).getRectangle().getHeight())
                .endY(pageHeight)
                .table(table.build())
                .build();

        final List<PDStream> streams = StreamSupport.stream(Spliterators.spliteratorUnknownSize(originalPage.getContentStreams(), Spliterator.ORDERED), false)
                .collect(Collectors.toList());
        tableDrawer.draw(() -> document, () -> {
                    PDPage page = new PDPage();
                    page.setMediaBox(originalPage.getMediaBox());
                    page.setCropBox(originalPage.getCropBox());
                    page.setResources(originalPage.getResources());
                    page.setRotation(originalPage.getRotation());
                    page.setContents(streams);
                    return page;
                },
                PDRectangle.A4.getHeight() - (columns.get(0).getWidgets().get(0).getRectangle().getLowerLeftY() +
                        columns.get(0).getWidgets().get(0).getRectangle().getHeight()));

        // Close the new content stream
        originalContentStream.close();

        if (sample != null) {
            removeField(document, sample.getFullyQualifiedName());
        }
    }

    private byte[] fillImpl(FilePart template, Map<String, Object> data, Map<String, Object> options, ArrayList<String> fallbackFontPaths) {
        try {
            PDDocument document = PDDocument.load(PDFToolsUtils.getInputStreamFromFlux(template.content()));
            PDDocumentCatalog catalog = document.getDocumentCatalog();
            PDAcroForm form = catalog.getAcroForm();

            // Force fontSize when required
            Integer forcedFontSize = null;
            try {
                forcedFontSize = (Integer) options.get("fontSize");
            } catch (Exception e1) {
                try {
                    forcedFontSize = Integer.parseInt((String) options.get("fontSize"), 10);
                } catch (Exception e2) {
                    if (options.get("fontSize").equals("auto")) {
                        forcedFontSize = 0;
                    }
                }
            }

            // Load fallback fonts
            LinkedHashMap<String, PDFont> fallbackFonts = new LinkedHashMap<>();
            for (String path : fallbackFontPaths) {
                PDFont font;
                if (path.endsWith(".otf")) {
                    OTFParser otfParser = new OTFParser();
                    OpenTypeFont otf = otfParser.parse(new FileInputStream(path));
                    font = PDType0Font.load(document, otf, false);
                } else {
                    font = PDType0Font.load(document, new FileInputStream(path), false);
                }
                PDResources res = form.getDefaultResources();
                fallbackFonts.put(res.add(font).getName(), font);
            }

            // Iterate through data
            if (form != null) {

                // Collect pageNum data
                float pageNumX = 0;
                float pageNumY = 0;
                PDFont pageNumFont = null;
                int pageNumFontSize = 0;
                PDField pageNumField = form.getField("#");
                if (pageNumField != null) {
                    PDRectangle pageNumRect = form.getField("#").getWidgets().get(0).getRectangle();
                    pageNumX = pageNumRect.getLowerLeftX() + pageNumRect.getWidth() / 4;
                    pageNumY = pageNumRect.getLowerLeftY() + pageNumRect.getHeight() / 4;

                    COSDictionary dict = pageNumField.getCOSObject();
                    COSString defaultAppearance = (COSString) dict.getDictionaryObject(COSName.DA);
                    Pattern pattern = Pattern.compile("/([A-Za-z]+) ([0-9]+) Tf");
                    Matcher matcher = pattern.matcher(defaultAppearance.toString());
                    LinkedHashMap<String, PDFont> fontMap = new LinkedHashMap<>();

                    PDPage page = document.getPage(0);
                    for (COSName fontName : page.getResources().getFontNames()) {
                        pageNumFont = page.getResources().getFont(fontName);
                        fontMap.put(fontName.getName(), pageNumFont);
                    }

                    pageNumFont = PDType1Font.HELVETICA;
                    if (matcher.find()) {
                        pageNumFontSize = Integer.parseInt(matcher.group(2));
                        String fontName = matcher.group(1);
                        if (fontMap.containsKey(fontName)) {
                            pageNumFont = fontMap.get(fontName);
                        }
                    }
                    removeField(document, "#");
                }

                // Iterate through data
                for (String key : data.keySet()) {
                    Object value = data.get(key);
                    // JSON
                    if (value instanceof ArrayList && ((ArrayList) value).size() > 0) {
                        if (form.getField(key) instanceof PDNonTerminalField) {
                            // Dynamic: Instead of filling, replace fields with static table
                            fillTable(document, form, key, value, fallbackFonts, forcedFontSize);
                        } else {
                            // Static: Just fill fields named in item[idx].field format
                            for (int idx = 0; idx < ((ArrayList) value).size(); idx++) {
                                Object item = ((ArrayList) value).get(idx);
                                if (item instanceof LinkedHashMap) {
                                    for (String key_ : ((LinkedHashMap<String, Object>) item).keySet()) {
                                        Object value_ = ((LinkedHashMap) item).get(key_);
                                        String key__ = String.format("%s[%d].%s", key, idx, key_);
                                        PDField field = form.getField(key__);
                                        if (field != null) {
                                            setFieldValue(field, value_, fallbackFonts, forcedFontSize);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        // Single field
                        PDField field = form.getField(key);
                        if (field != null && value instanceof String && ((String) value).startsWith("qrcode:")) {
                            PDAnnotationWidget widget = field.getWidgets().get(0);
                            if (widget != null) {
                                PDRectangle rect = widget.getRectangle();
                                removeField(document, "#");
                                try {
                                    PDImageXObject qrCodeImage = createQRCodeImage(document, ((String) value).substring(7),
                                            Math.round(rect.getWidth()), Math.round(rect.getHeight())
                                    );
                                    float imageWidth = qrCodeImage.getWidth();
                                    float imageHeight = qrCodeImage.getHeight();
                                    PDPage page = document.getPage(0);
                                    PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true, true);
                                    contentStream.drawImage(qrCodeImage, rect.getLowerLeftX(), rect.getLowerLeftY(), imageWidth, imageHeight);
                                    contentStream.close();
                                    removeField(document, field.getFullyQualifiedName());
                                } catch (WriterException e) {
                                    log.error(e.toString());
                                }
                            }
                        } else if (field != null) {
                            setFieldValue(field, value, fallbackFonts, forcedFontSize);
                        }
                    }
                }
                if (pageNumX > 0 && pageNumY > 0) {
                    fillPageNumber(document, pageNumX, pageNumY, pageNumFont, pageNumFontSize);
                }
                Path tempDir;
                tempDir = Files.createTempDirectory("tmpPDF");
                File outputFile;
                outputFile = new File(tempDir.toUri().getPath() + template.filename());
                try {
                    document.save(outputFile);
                    document.close();
                    return FileUtils.readFileToByteArray(outputFile);
                } finally {
                    outputFile.delete();
                    Files.delete(tempDir);
                }
            }
        } catch (IOException e) {
            log.error(e);
        }
        return null;
    }
    public static float extractFontSize(COSString defaultAppearance) {
        if (defaultAppearance != null && !defaultAppearance.getASCII().isEmpty()) {
            // Split the string by space to get individual tokens
            String[] tokens = defaultAppearance.getASCII().split("\\s+");

            // Look for the Tf operator and get the preceding value as font size
            for (int i = 0; i < tokens.length; i++) {
                if ("Tf".equals(tokens[i])) {
                    try {
                        // The token before "Tf" is the font size
                        return Float.parseFloat(tokens[i - 1]);
                    } catch (NumberFormatException e) {
                        // Handle potential parsing error
                        System.err.println("Error parsing font size: " + e.getMessage());
                    }
                }
            }
        }
        return -1;
    }
}
