package fi.jyu.vasara.pdftools.handler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.codec.DecodingException;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DataBufferLimitException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.UnsupportedMediaTypeStatusException;
import reactor.core.publisher.Mono;

@Configuration
@Order(-2)
@Log4j2
public class GlobalErrorHandler implements ErrorWebExceptionHandler {

    private final ObjectMapper objectMapper;

    public GlobalErrorHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public Mono<Void> handle(ServerWebExchange serverWebExchange, Throwable throwable) {
        log.error(throwable.getMessage(), throwable);

        DataBufferFactory bufferFactory = serverWebExchange.getResponse().bufferFactory();

        if (
                throwable instanceof DecodingException ||
                throwable instanceof UnsupportedMediaTypeStatusException
        ) {
            return getMono(serverWebExchange, throwable, bufferFactory, HttpStatus.BAD_REQUEST);
        }

        if (throwable instanceof DataBufferLimitException) {
            return getMono(serverWebExchange, throwable, bufferFactory, HttpStatus.PAYLOAD_TOO_LARGE);
        }

        // Unhandled exceptions
        serverWebExchange.getResponse().setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
        serverWebExchange.getResponse().getHeaders().setContentType(MediaType.TEXT_PLAIN);
        DataBuffer dataBuffer = bufferFactory.wrap("Technical error".getBytes());
        return serverWebExchange.getResponse().writeWith(Mono.just(dataBuffer));
    }

    private Mono<Void> getMono(ServerWebExchange serverWebExchange, Throwable throwable, DataBufferFactory bufferFactory, HttpStatus status) {
        serverWebExchange.getResponse().setStatusCode(status);
        DataBuffer dataBuffer;
        try {
            dataBuffer = bufferFactory.wrap(objectMapper.writeValueAsBytes(new HttpError(throwable.getMessage())));
        } catch (JsonProcessingException e) {
            dataBuffer = bufferFactory.wrap("".getBytes());
        }
        serverWebExchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        return serverWebExchange.getResponse().writeWith(Mono.just(dataBuffer));
    }

    public static class HttpError {

        private final String message;

        HttpError(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

}