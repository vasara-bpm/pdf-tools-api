package fi.jyu.vasara.pdftools.exception;

public class EmptyResponseException extends Exception {
    public EmptyResponseException(String msg) {
        super(msg);
    }
}
