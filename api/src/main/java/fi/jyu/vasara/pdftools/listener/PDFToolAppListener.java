package fi.jyu.vasara.pdftools.listener;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.verapdf.pdfa.VeraGreenfieldFoundryProvider;

@Component
@Order(0)
@Log4j2
public class PDFToolAppListener implements ApplicationListener<ApplicationReadyEvent> {

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        VeraGreenfieldFoundryProvider.initialise();
        log.info("VeraGreenfieldFoundryProvider initialized");
    }
}
