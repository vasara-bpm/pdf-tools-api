package fi.jyu.vasara.pdftools.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.log4j.Log4j2;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.interactive.form.*;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.verapdf.core.ValidationException;
import org.verapdf.metadata.fixer.utils.ProcessedObjectsInspector;
import org.verapdf.metadata.fixer.utils.ValidationStatus;
import org.verapdf.pdfa.Foundries;
import org.verapdf.pdfa.PDFAParser;
import org.verapdf.pdfa.PDFAValidator;
import org.verapdf.pdfa.results.TestAssertion;
import org.verapdf.pdfa.results.ValidationResult;
import org.verapdf.pdfa.validation.profiles.ValidationProfile;
import org.xml.sax.SAXException;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

@Log4j2
public class PDFToolsUtils {
    public static InputStream getInputStreamFromFlux(Flux<DataBuffer> data) throws IOException {
        PipedOutputStream osPipe = new PipedOutputStream();
        PipedInputStream isPipe = new PipedInputStream(osPipe);

        DataBufferUtils.write(data, osPipe)
                .subscribeOn(Schedulers.boundedElastic())
                .doOnComplete(() -> {
                    try {
                        osPipe.close();
                    } catch (IOException ignored) {
                    }
                })
                .subscribe(DataBufferUtils.releaseConsumer());

        return isPipe;
    }

    public static ValidationResult getValidationResult(PDFAParser parser) throws ValidationException {
        // Automatic pdf/a compatibility selection
        PDFAValidator validator = Foundries.defaultInstance().createValidator(parser.getFlavour(), false);

        return validator.validate(parser);
    }

    public static ValidationStatus getValidationStatus(ValidationResult validationResult) throws IOException, ParserConfigurationException, SAXException, URISyntaxException {
        List<TestAssertion> testAssertions = validationResult.getTestAssertions();
        ValidationProfile validationProfile = validationResult.getValidationProfile();

        return ProcessedObjectsInspector.validationStatus(testAssertions, validationProfile);
    }

    public static String printFields(PDAcroForm acroForm) throws IOException {
        List<PDField> fields = acroForm.getFields();

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode objectNode = objectMapper.createObjectNode();

        for (PDField field : fields) {
            processField(field, field.getPartialName(), objectNode);
        }

        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(objectNode);
    }

    private static void processField(PDField field, String sParent, ObjectNode objectNode) throws IOException {
        String partialName = field.getPartialName();

        if (field instanceof PDNonTerminalField) {
            if (!sParent.equals(field.getPartialName()) && partialName != null) {
                sParent = partialName;
            }

            ObjectNode childNode = new ObjectMapper().createObjectNode();
            objectNode.set(sParent, childNode);

            for (PDField child : ((PDNonTerminalField) field).getChildren()) {
                processField(child, sParent, childNode);
            }
        } else {
            ObjectNode childNode = new ObjectMapper().createObjectNode();
            String fieldValue = field.getValueAsString();

            /*
             * Since PDField.getFieldType() returns field dictionary FT value (Btn, Tx, Ch, Sig)
             * we will get PDFBox class name without additional manipulations as the most readable solution.
             */
            String className = field.getClass().getName();
            String fieldType = className.substring(className.lastIndexOf('.') + 1);

            childNode.put("type", fieldType);
            childNode.put("required", field.isRequired());
            childNode.put("readOnly", field.isReadOnly());
            childNode.put("value", fieldValue);

            if (field instanceof PDTextField) {
                childNode.put("default", ((PDTextField) field).getDefaultValue());

                int maxLength = ((PDTextField) field).getMaxLen();

                if (maxLength != -1) {
                    childNode.put("maxLength", maxLength);
                }
            }

            if (field instanceof PDChoice) {
                childNode.put("default", ((PDChoice) field).getDefaultValue().toString());

                if (!((PDChoice) field).isCombo()) {
                    childNode.put("multiple", ((PDChoice) field).isMultiSelect());
                }

                List<String> optionList = ((PDChoice) field).getOptions();

                if (!optionList.isEmpty()) {
                    SortedSet<String> sortedSet = new TreeSet<>(optionList);

                    if (!sortedSet.isEmpty()) {
                        childNode.put("options", sortedSet.toString());
                    }
                }
            }

            if (field instanceof PDRadioButton) {
                childNode.put("noToggleOff", field.getCOSObject().getFlag(COSName.FF, 1 << 14));
                List<String> selectedExportValues = ((PDRadioButton) field).getSelectedExportValues();
                childNode.put("options", Arrays.toString(new String[]{"Off", String.join(",", selectedExportValues)}));
            }

            if (field instanceof PDCheckBox) {
                childNode.put("options", Arrays.toString(new String[]{"Off", ((PDCheckBox) field).getOnValue()}));
            }

            if (partialName != null) {
                objectNode.set(partialName, childNode);
            } else {
                objectNode.set("", childNode);
            }
        }
    }

    public static void setField(PDField field, String name, String value) throws IOException {
        if (field != null && !field.isReadOnly()) {
            if (field instanceof PDCheckBox) {
                PDCheckBox checkbox = (PDCheckBox) field;
                if (value.isEmpty()) {
                    checkbox.unCheck();
                } else {
                    checkbox.check();
                }
            } else if (field instanceof PDComboBox) {
                String[] array = stringValueToArray(value);
                field.setValue(array[0]);
            } else if (field instanceof PDListBox) {
                String[] array = stringValueToArray(value);

                if (((PDListBox) field).isMultiSelect()) {
                    List<String> values = Arrays.asList(array);
                    ((PDListBox) field).setValue(values);
                } else {
                    field.setValue(array[0]);
                }
            } else if (field instanceof PDRadioButton) {
                field.setValue(value);
            } else if (field instanceof PDTextField) {
                field.setValue(value);
            }
        } else {
            System.err.println("No field found with name: " + name);
        }
    }

    private static String[] stringValueToArray(String value) {
        return value.replaceAll("[\\[\\]]", "").split(",");
    }
}
