package fi.jyu.vasara.pdftools.service;

import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;

import java.util.List;
import java.util.Map;

public interface PDFTools {
    ResponseEntity<byte[]> convert(FilePart pdfFile);
    ResponseEntity<String> extract(FilePart pdfFile);
    ResponseEntity<byte[]> fill(FilePart pdfFile, List<Map<String, Object>> formFields, Map<String, Object> options);
}
