/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.pdfbox.pdmodel.interactive.form;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAppearanceStream;

import java.io.IOException;

public class PDFontSizeCalculator {
    private static final Log LOG = LogFactory.getLog(PDFontSizeCalculator.class);
    private final PDVariableText field;

    private PDDefaultAppearanceString defaultAppearance;
    private String value;

    /**
     * The scaling factor for font units to PDF units
     */
    private static final int FONTSCALE = 1000;

    /**
     * The default font size used for multiline text
     */
    private static final float DEFAULT_FONT_SIZE = 12;

    /**
     * The minimum/maximum font sizes used for multiline text auto sizing
     */
    private static final float MINIMUM_FONT_SIZE = 4;

    /**
     * Constructs a COSAppearance from the given field.
     *
     * @param field the field which you wish to control the appearance of
     * @throws IOException
     */
    public PDFontSizeCalculator(PDVariableText field, String value) throws IOException {
        this.field = field;
        // Treat multiline field values in single lines as single lime values.
        // This is in line with how Adobe Reader behaves when entering text
        // interactively but NOT how it behaves when the field value has been
        // set programmatically and Reader is forced to generate the appearance
        // using PDAcroForm.setNeedAppearances
        // see PDFBOX-3911
        if (field instanceof PDTextField && !((PDTextField) field).isMultiline()) {
            this.value = value.replaceAll("\\u000D\\u000A|[\\u000A\\u000B\\u000C\\u000D\\u0085\\u2028\\u2029]", " ");
        } else if (value == null) {
            this.value = "";
        } else {
            this.value = value;
        }
        validateAndEnsureAcroFormResources();

        try {
            this.defaultAppearance = field.getDefaultAppearanceString();
        } catch (IOException ex) {
            throw new IOException("Could not process default appearance string '" + field.getDefaultAppearance()
                    + "' for field '" + field.getFullyQualifiedName() + "'", ex);
        }
    }

    /**
     * My "not so great" method for calculating the fontsize. It does not work
     * superb, but it handles ok.
     *
     * @return the calculated font-size
     * @throws IOException If there is an error getting the font information.
     */
    public float calculateFontSize(PDRectangle contentRect) throws IOException {
        float fontSize = defaultAppearance.getFontSize();
        PDFont font = defaultAppearance.getFont();

        // zero is special, it means the text is auto-sized
        if (fontSize == 0) {
            if (isMultiLine()) {
                PlainText textContent = new PlainText(value);
                if (textContent.getParagraphs() != null) {
                    float width = contentRect.getWidth(); //  - contentRect.getLowerLeftX();
                    float fs = MINIMUM_FONT_SIZE;
                    while (fs <= DEFAULT_FONT_SIZE) {
                        // determine the number of lines needed for this font and contentRect
                        int numLines = 0;
                        for (PlainText.Paragraph paragraph : textContent.getParagraphs()) {
                            numLines += paragraph.getLines(font, fs, width).size();
                        }
                        // calculate the height required for this font size
                        float fontScaleY = fs / FONTSCALE;
                        float leading = font.getBoundingBox().getHeight() * fontScaleY;
                        float height = leading * numLines;

                        // if this font size didn't fit, use the prior size that did fit
                        if (height > contentRect.getHeight()) {
                            return Math.max(fs - 1, MINIMUM_FONT_SIZE);
                        }
                        fs++;
                    }
                    return Math.min(fs, DEFAULT_FONT_SIZE);
                }

                // Acrobat defaults to 12 for multiline text with size 0
                return DEFAULT_FONT_SIZE;
            } else {
                float yScalingFactor = FONTSCALE * font.getFontMatrix().getScaleY();
                float xScalingFactor = FONTSCALE * font.getFontMatrix().getScaleX();

                // fit width
                float width = font.getStringWidth(value) * font.getFontMatrix().getScaleX();
                float widthBasedFontSize = contentRect.getWidth() / width * xScalingFactor;

                // fit height
                float height = (font.getFontDescriptor().getCapHeight() + -font.getFontDescriptor().getDescent())
                        * font.getFontMatrix().getScaleY();
                if (height <= 0) {
                    height = font.getBoundingBox().getHeight() * font.getFontMatrix().getScaleY();
                }

                float heightBasedFontSize = contentRect.getHeight() / height * yScalingFactor;

                return Math.min(heightBasedFontSize, widthBasedFontSize);
            }
        }
        return fontSize;
    }

    /*
     * Adobe Reader/Acrobat are adding resources which are at the field/widget level
     * to the AcroForm level.
     */
    private void validateAndEnsureAcroFormResources() {
        // add font resources which might be available at the field
        // level but are not at the AcroForm level to the AcroForm
        // to match Adobe Reader/Acrobat behavior
        PDResources acroFormResources = field.getAcroForm().getDefaultResources();
        if (acroFormResources == null)
        {
            return;
        }

        for (PDAnnotationWidget widget : field.getWidgets())
        {
            PDAppearanceStream stream = widget.getNormalAppearanceStream();
            if (stream == null)
            {
                continue;
            }
            PDResources widgetResources = stream.getResources();
            if (widgetResources == null)
            {
                continue;
            }
            COSDictionary widgetFontDict = widgetResources.getCOSObject()
                    .getCOSDictionary(COSName.FONT);
            COSDictionary acroFormFontDict = acroFormResources.getCOSObject()
                    .getCOSDictionary(COSName.FONT);
            for (COSName fontResourceName : widgetResources.getFontNames())
            {
                try
                {
                    if (acroFormResources.getFont(fontResourceName) == null)
                    {
                        LOG.debug("Adding font resource " + fontResourceName + " from widget to AcroForm");
                        // use the COS-object to preserve a possible indirect object reference
                        acroFormFontDict.setItem(fontResourceName,
                                widgetFontDict.getItem(fontResourceName));
                    }
                }
                catch (IOException e)
                {
                    LOG.warn("Unable to match field level font with AcroForm font");
                }
            }
        }
    }
    private boolean isMultiLine() {
        return field instanceof PDTextField && ((PDTextField) field).isMultiline();
    }
}
