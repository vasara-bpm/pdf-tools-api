SHELL := /usr/bin/env bash
export PATH := node_modules/.bin:$(PATH)

SOURCES = $(shell find . -name "*.java" -or -name "*.xml" -or -name "*.yaml")

.PHONY: all
all: test

.PHONY: test
test:
	mvn test

.PHONY: shell
shell:
	nix develop

###

.PHONY: nix-%
nix-%:
	nix develop $(NIX_OPTIONS) --command $(MAKE) $*

mvn2nix-lock.json: pom.xml
	mvn2nix -vvv > mvn2nix-lock.json

include release-container.mk
